Dependencies
----------
- Point Cloud Library: libpcl-dev on debian
- CMake

Compilation
-----------
- cmake .
- make

Usage
-----
./interactive_icp pointcloud1 pointcloud2 number_of_ICP_iteration threshold

Example
-------
./interactive_icp depth1.txt depth.txt 300 1000

.. image:: example.png
